package similarity;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.paul.annotations.AncestorAnnotation;
import de.paul.annotations.Annotatable;
import de.paul.annotations.Category;
import de.paul.annotations.NeighborhoodAnnotation;
import de.paul.documents.AnnotatedDoc;
import de.paul.kb.dbpedia.DBPediaHandler;
import de.paul.kb.dbpedia.categories.WikiCatHierarchyHandler;
import de.paul.util.Directionality;
import de.paul.util.Paths;
import ontologyManagement.MyOWLIndividual;
import ontologyManagement.MyOWLLogicalEntity;
import ontologyManagement.MyOWLOntology;
import ontologyManagement.OWLConcept;
import ontologyManagement.OWLLink;
import test.ComparisonResult;
import test.DatasetTest;
import test.DatasetTestCombined;

public class InformationContent {
	private static InformationContent instance = null;
	public static InformationContent getInstance() throws Exception
	{
		if (instance == null)
		{
			throw new Exception("No InformationContent instance created. You should call the constructor before calling this method.");
		}
		return instance;
	}
	
	//private Map<OWLConcept, Integer> occurrences;
	private Map<MyOWLLogicalEntity, Set<Entity>> occurrences;
	private Map<String, Set<AnnotatedDoc>> paulOccurrences;
	private Map<String, Double> ics;
	private int totalAnnotations;
	private double maxIC;
	
	public InformationContent(List<AnnotatedDoc> docs, MyOWLOntology o)
	{
		instance = this;
		paulOccurrences = new HashMap<String, Set<AnnotatedDoc>>();
		ics = new HashMap<String, Double>();
		totalAnnotations = docs.size();
		for (AnnotatedDoc d: docs)
		{
			List<Annotatable> anns = d.getAnnotations();
			Set<String> annStr = new HashSet<String>();
			for (Annotatable a: anns)
			{
				annStr.add(a.getEntity());
				/*NeighborhoodAnnotation neighbors = new NeighborhoodAnnotation(a.getEntity(), a.getWeight(),
						DBPediaHandler.getInstance(Paths.TDB_DBPEDIA), 1, Directionality.OUTGOING);
				for (Annotatable l: neighbors.getNeighbors())
				{
					annStr.add(l.getEntity());
				}*/
				MyOWLIndividual individual = o.getMyOWLIndividual(a.getEntity());
				Set<OWLLink> links = individual.getNeighbors();
				for (OWLLink l: links)
				{
					annStr.add(l.getDestiny().getURI());
				}
				
			}
			for (String a: annStr)
			{
				Set<AnnotatedDoc> aux = paulOccurrences.get(a);
				if (aux == null)
				{
					aux = new HashSet<AnnotatedDoc>();
					paulOccurrences.put(a, aux);
				}
				aux.add(d);
				AncestorAnnotation aa = new AncestorAnnotation(a, 1,
					DBPediaHandler.getInstance(Paths.TDB_DBPEDIA),
					WikiCatHierarchyHandler.getInstance(Paths.TDB_DBPEDIA_HIERARCHY));
				Set<Category> ancestors = aa.getAncestors();
				for (Category c: ancestors)
				{
					aux = paulOccurrences.get(c.getName());
					if (aux == null)
					{
						aux = new HashSet<AnnotatedDoc>();
						paulOccurrences.put(c.getName(), aux);
					}
					aux.add(d);
				}
				
			}
		}
		for (String a: paulOccurrences.keySet())
		{
			this.setIC(a);
		}
		maxIC = Collections.max(ics.values());
	}
	
	
	
	static class Entity{
		private String name;
		private static Map<String, Entity> entMap = new HashMap<String, Entity>();
		
		public Entity(String n)
		{
			name = n;
		}
		
		public static Entity getEntity(String n)
		{
			Entity e = entMap.get(n);
			if (e == null)
				e = new Entity(n);
			return e;
		}
		
		public String getName()
		{
			return name;
		}
		
		public boolean equals(Object o)
		{
			return equals((Entity) o);
		}
		
		public boolean equals(Entity a)
		{
			return name.matches(a.name);
		}
		
		public int hashCode()
		{
			return name.hashCode();
		}
	}
	
	private static Set<OWLConcept> getLeafNodes(Set<OWLConcept> conceptsOrig)
	{
		Set<OWLConcept> concepts = new HashSet<OWLConcept>(conceptsOrig);
		Set<OWLConcept> auxSet = new HashSet<OWLConcept>(concepts);
		for (OWLConcept c: auxSet)
		{
			for (OWLConcept d: auxSet)
			{
				if (!c.equals(d))
				{
					if (d.isSubConceptOf(c))
						concepts.remove(c);
				}
			}
		}
		return concepts;
	}
	
	
	/*protected void setIC(MyOWLLogicalEntity c)
	{
		double freq = 0;
		Set<OWLConcept> subConcepts = Collections.emptySet();
		if (c instanceof OWLConcept)
		{	
			subConcepts = ((OWLConcept) c).getSubConcepts();
			subConcepts.add((OWLConcept) c);
		}
			
		Set<Entity> union = new HashSet<Entity>();
		if (c instanceof OWLConcept)
		{
			for (Iterator<OWLConcept> i = subConcepts.iterator(); i.hasNext();)
			{
				OWLConcept a = i.next();
				//Integer aux = occurrences.get(a);
				Set<Entity> aux = occurrences.get(a);
				if (aux == null)
					aux = Collections.emptySet();//0;
				union.addAll(aux);
				//freq += aux;
			}
		}
		else
			union = occurrences.get(c);
		freq = union.size();
		if (freq/totalAnnotations > 1)
			System.out.println(c);
		ics.put(c, -Math.log(freq/totalAnnotations));
	}*/
	
	protected void setIC(String c)
	{
		double freq = 0;
		Set<AnnotatedDoc> docs = paulOccurrences.get(c);
		if (docs == null)
			docs = Collections.emptySet();
		
		freq = docs.size();
		if (freq/totalAnnotations > 1)
			System.out.println(c);
		ics.put(c, -Math.log(freq/totalAnnotations));
	}
	
	public double getIC(String c)
	{
		Double ic = ics.get(c); 
		if ( ic == null)
		{
			/*System.out.println("ERROR: No IC computed");
			setIC(c);
			ic = ics.get(c);*/
			return 1;
		}
		//double maxIC = Collections.max(ics.values());
		return ic/maxIC;
	}

}
