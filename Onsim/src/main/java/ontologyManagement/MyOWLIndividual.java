package ontologyManagement;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLNamedIndividual;

import de.paul.annotations.AncestorAnnotation;
import de.paul.db.JSONDocSourceLoader;
import de.paul.documents.AnnotatedDoc;
import de.paul.kb.dbpedia.DBPediaHandler;
import de.paul.kb.dbpedia.categories.WikiCatHierarchyHandler;
import de.paul.similarity.entityScorers.CommonAncestor;
import de.paul.similarity.entityScorers.LCAScorer;
import de.paul.util.Paths;
import similarity.ComparableElement;
import similarity.InformationContent;
import similarity.matching.AnnSim;
import similarity.matching.BipartiteGraphMatching;

public class MyOWLIndividual extends MyOWLLogicalEntity {
	private OWLNamedIndividual ind;
	private Map<MyOWLIndividual, Double> ownSimilarities;
	private Map<MyOWLIndividual, Double> taxSimilarities;
	private Map<MyOWLIndividual, Double> icSimilarities;
	private static Map<MyOWLIndividual, Map<MyOWLIndividual, Double>> similarities = new HashMap<MyOWLIndividual, Map<MyOWLIndividual, Double>>();
	private double weight = 0.0;
	private AncestorAnnotation aa;

	public MyOWLIndividual(OWLNamedIndividual a, MyOWLOntology onto) {
		o = onto;
		uri = a.getIRI().toURI().toString();
		neighbors = null;
		ind = a;
		ownSimilarities = new HashMap<MyOWLIndividual, Double>();
		taxSimilarities = new HashMap<MyOWLIndividual, Double>();
		icSimilarities = new HashMap<MyOWLIndividual, Double>();
		similarities.put(this, ownSimilarities);
		aa = new AncestorAnnotation(this.uri, this.weight,
				DBPediaHandler.getInstance(Paths.TDB_DBPEDIA),
				WikiCatHierarchyHandler.getInstance(Paths.TDB_DBPEDIA_HIERARCHY));
	}

	public OWLNamedIndividual getOWLNamedIndividual() {
		if (ind == null)
			ind = o.getOWLIndividual(uri).asOWLNamedIndividual();
		return ind;
	}
	
	public AncestorAnnotation getAncestorAnnotation()
	{
		return aa;
	}

	public double similarityNeighbors(MyOWLIndividual c) {
		//BipartiteGraphMatching bpm = new BipartiteGraphMatching();
		AnnSim bpm = new AnnSim();
		if (neighbors == null)
			neighbors = o.getIndividualOWLLink(this);
		if (c.neighbors == null)
			c.neighbors = o.getIndividualOWLLink(c);
		try {
			double sim;
			if (neighbors.isEmpty() && c.neighbors.isEmpty())// && !this.equals(c))
				sim = -1;
			else
				//sim = bpm.matching(neighbors, c.neighbors, this, c);
				sim = bpm.maximumMatching(neighbors, c.neighbors, this, c, null);
			return sim;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0.0;
	}
	
	public double similarityIC(MyOWLIndividual c)
	{
		Double sim = icSimilarities.get(c);
		if (sim == null)
		{
			try {
				InformationContent ic = InformationContent.getInstance();
				CommonAncestor canc = aa.lowestCommonAncestor(c.aa);
				if (canc == null)
					sim = 0.0;
				else
					sim = ic.getIC(canc.getName());
				icSimilarities.put(c, sim);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sim;
	}

	@Override
	protected double similarityNeighbors(MyOWLLogicalEntity c) throws Exception {
		if (c instanceof MyOWLIndividual) {
			MyOWLIndividual ind = (MyOWLIndividual) c;
			return this.similarityNeighbors(ind);
		}
		return 0;
	}

	public double taxonomicSimilarity(MyOWLIndividual c) {
		if (taxSimilarities.get(c) == null)
		{			
			LCAScorer scorer = new LCAScorer(aa, c.aa);
			scorer.setLeft();
			taxSimilarities.put(c, scorer.score());
		}
		return taxSimilarities.get(c);
		/*CommonAncestor lca = a.lowestCommonAncestor(b);
		if (lca == null) {
			return 0.0;
		} else {
			double dtax = lca.dpsScore();
			return 1 - dtax;
		}*/
		// return o.taxonomicIndividualSimilarity(getOWLNamedIndividual(),
		// c.getOWLNamedIndividual());
	}

	public double taxonomicSimilarity(MyOWLLogicalEntity c) throws Exception {
		if (!(c instanceof MyOWLIndividual))
			throw new Exception("Invalid comparison between " + this + " and " + c);
		return taxonomicSimilarity((MyOWLIndividual) c);
	}

	public boolean isOfType(OWLConcept c) {
		return o.isOfType(getOWLNamedIndividual(), c.getOWLClass());
	}

	public double getIC() {
		double icont = 0;
		try {
			InformationContent ic = InformationContent.getInstance();
			icont = ic.getIC(uri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return icont;
	}

	public double getDepth() {
		return o.prof(this.ind);
	}

	public double similarity(MyOWLIndividual c) {
		if (this == c)
			return 1.0;
		Double sim = ownSimilarities.get(c);
		if (sim != null)
			return sim;

		//double taxSim = taxonomicSimilarity(c);
		double neighSim = 1;
		// if (taxSim > 0)
		if (!this.neighbors.isEmpty() && !c.neighbors.isEmpty())
			neighSim = similarityNeighbors(c);
		else
			neighSim = 0;
		// System.out.println(this + "\t" + c + "\t" + taxSim + "\t" +
		// neighSim);
		// System.out.println(this + "\t" + c + "\t" + taxSim + "\t" +
		// taxSim*neighSim);
		sim = /*taxSim;*/ neighSim;

		ownSimilarities.put(c, sim);
		c.ownSimilarities.put(this, sim);
		return sim;
	}

	public double similarity(ComparableElement a, MyOWLIndividual org, MyOWLIndividual des) throws Exception {
		if (!(a instanceof OWLConcept))
			throw new Exception("Invalid comparison between " + this + " and " + a);
		return similarity((MyOWLIndividual) a);
	}

	public OWLConcept getLCA(MyOWLIndividual b) {
		return o.getLCS(this, b);
	}

	public double similarity(ComparableElement a, MyOWLLogicalEntity org, MyOWLLogicalEntity des) throws Exception {
		if (!(a instanceof MyOWLIndividual))
			throw new Exception("Invalid comparison between " + this + " and " + a);
		return similarity((MyOWLIndividual) a);
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setNeighbors(Set<OWLLink> n) {
		Set<MyOWLLogicalEntity> set = new HashSet<MyOWLLogicalEntity>();
		for (OWLLink l: n)
		{
			set.add(l.getDestiny());
		}
		/*if (set.size() != n.size())
			System.out.println("Interesting: " + set.size() + " " + n.size());*/
		neighbors = n;
	}

	public Set<OWLLink> getNeighbors() {
		if (neighbors == null)
			neighbors = o.getIndividualOWLLink(this);
		return neighbors;
	}
}
