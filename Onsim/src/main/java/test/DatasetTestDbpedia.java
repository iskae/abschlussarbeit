package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFWriter;
import org.apache.jena.riot.RDFDataMgr;

import ontologyManagement.MyOWLIndividual;
import ontologyManagement.MyOWLOntology;

public class DatasetTestDbpedia {

	public static Set<MyOWLIndividual> getOntologyTerms(List<ComparisonResult> comparisons, String[] files,
			MyOWLOntology m) {
		Set<String> proteins = new HashSet<String>();
		Set<MyOWLIndividual> annotations = new HashSet<MyOWLIndividual>();
		for (Iterator<ComparisonResult> i = comparisons.iterator(); i.hasNext();) {
			ComparisonResult indComp = i.next();
			proteins.add(indComp.getConceptA());
			proteins.add(indComp.getConceptB());
		}
		for (Iterator<String> i = proteins.iterator(); i.hasNext();) {
			String p = i.next();
			for (String file : files) {
				annotations.addAll(DatasetTestDbpedia.getIndividualAnnotations(p, file, m));
			}
		}
		return annotations;
	}

	public static Set<MyOWLIndividual> getIndividualAnnotations(String textDoc, String folder, MyOWLOntology m) {
		File f = new File(folder + "/" + textDoc);
		if (f.exists()) {
			return getAnnotations(f, m);
		}
		return new HashSet<MyOWLIndividual>();
	}

	public static Set<MyOWLIndividual> getAnnotations(File f, MyOWLOntology m) {
		InputStream fis;
		BufferedReader br;
		String line;

		Set<MyOWLIndividual> annotations = new HashSet<MyOWLIndividual>();

		try {
			fis = new FileInputStream(f);
			br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
			line = br.readLine(); // First line contains the number of
									// annotations
			int numAnnotations = Integer.parseInt(line);
			for (int i = 0; i < numAnnotations; i++) {
				line = br.readLine();
				String term = line.split("\t")[0];
				MyOWLIndividual ind = m.getMyOWLIndividual(term);
				if (ind != null)
					annotations.add(ind);
			}

			// Done with the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return annotations;
	}

	public static List<ComparisonResult> readComparisonFile(String comparisonFile) {
		List<ComparisonResult> comparisons = new ArrayList<ComparisonResult>();

		InputStream fis;
		BufferedReader br;
		String line;

		try {
			fis = new FileInputStream(comparisonFile);
			br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\t");
				comparisons.add(new ComparisonResult(elements[0], elements[1]));
			}

			// Done with the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comparisons;
	}

	public static void main(String[] args) throws Exception {
		Model m = ModelFactory.createOntologyModel();

		File ontFile = new File("dbpediaOwl.owl");
		String prefix = ("src/main/resources/dbpedia/");
		JSONHandler jHandler = new JSONHandler(prefix + "Pincombe_annotated_xLisa_corrected.json");
		QueryHandler qHandler = new QueryHandler();
		FileHandler fHandler = new FileHandler();
		// Get individuals from json file and insert them to ontmodel.
		Set<String> individuals = jHandler.getIndividuals();

		for (String individual : individuals) {
			System.out.println(individual);
			m = qHandler.executeQuery(qHandler.tripleQuery(individual), qHandler.ancestorsQuery(individual), m);
		}

		RDFWriter fasterWriter = m.getWriter();
		fasterWriter.setProperty("allowBadURIs", "true");
		fasterWriter.setProperty("relativeURIs", "");
		fasterWriter.setProperty("tab", "1");
		OutputStream out = new FileOutputStream("dbpediaOwl.owl");
		fasterWriter.write(m, out, " ");
		// Get Text Documents with annotations.

		Map<TextDoc, List<TextDocAnnotation>> documents = jHandler.getDocs();
		fHandler.processAnnotations(documents, prefix + "process_annt/");
		fHandler.generatePossiblePairs(documents, prefix);
		MyOWLOntology o = new MyOWLOntology(ontFile.getAbsolutePath(), prefix);
		String comparisonFile = prefix + "textPairs.txt";
		List<ComparisonResult> comparisons = fHandler.readComparisonFile(comparisonFile);

		String[] files = { prefix + "process_annt" };

		Set<MyOWLIndividual> annotations = DatasetTestDbpedia.getOntologyTerms(comparisons, files, o);
		Set<IndividualComparison> individualComparisons = new HashSet<IndividualComparison>();
		for (Iterator<ComparisonResult> i = comparisons.iterator(); i.hasNext();) {
			ComparisonResult comp = i.next();
			for (String file : files) {
				Set<MyOWLIndividual> a = getIndividualAnnotations(comp.getConceptA(), file, o);
				Set<MyOWLIndividual> b = getIndividualAnnotations(comp.getConceptB(), file, o);
				for (MyOWLIndividual c1 : a) {
					for (MyOWLIndividual c2 : b) {
						individualComparisons.add(new IndividualComparison(c1, c2));
					}
				}
			}
		}

		System.out.println("Individual Comparisons: " + individualComparisons.size());
		Map<IndividualComparison, Double> costMatrix = new HashMap<IndividualComparison, Double>();
		for (IndividualComparison comparison : individualComparisons) {
			Double sim = comparison.getIndividualA().similarity(comparison.getIndividualB());
			costMatrix.put(comparison, sim);
		}

		PrintWriter generalWriter = new PrintWriter(prefix + "comparisons.txt", "UTF-8");
		Map<String, PrintWriter> writers = new HashMap<String, PrintWriter>();
		for (String file : files) {
			writers.put(file, new PrintWriter(prefix + file.replaceAll(prefix, "") + "results.txt"));
		}
		// generalWriter.println("Protein1\tProtein2\tSimilarity");
		int counter = 0, total = comparisons.size();
	}

}
