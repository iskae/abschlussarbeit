package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.paul.annotations.Annotatable;
import de.paul.documents.AnnotatedDoc;
import ontologyManagement.MyOWLIndividual;
import ontologyManagement.MyOWLOntology;

public class FileHandler {

	public FileHandler() {

	}

	public File createTextDocAnnotationsFile(String path) throws IOException {
		File file = new File(path);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		return file;
	}

	public void processAnnotations(Map<TextDoc, List<TextDocAnnotation>> documents, String pathPrefix)
			throws IOException {
		for (Entry<TextDoc, List<TextDocAnnotation>> doc : documents.entrySet()) {
			TextDoc docToProcess = doc.getKey();
			List<TextDocAnnotation> annotationsInDoc = doc.getValue();
			File f = createTextDocAnnotationsFile(pathPrefix + docToProcess.getId());
			try {
				PrintWriter annsWriter = new PrintWriter(f);
				annsWriter.println(annotationsInDoc.size());
				for (TextDocAnnotation annotation : annotationsInDoc) {
					annsWriter.println(annotation.getUri());
				}
				annsWriter.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
	}

	public void generatePossiblePairs(Map<TextDoc, List<TextDocAnnotation>> documents, String pathPrefix)
			throws IOException {
		List<Integer> ids = new LinkedList<Integer>();
		for (Entry<TextDoc, List<TextDocAnnotation>> doc : documents.entrySet()) {
			TextDoc document = doc.getKey();
			int id = document.getId();
			ids.add(id);
		}
		Object[] idsAsArray = ids.toArray();
		File f = createTextDocAnnotationsFile(pathPrefix + "textPairs.txt");
		PrintWriter pairWriter = new PrintWriter(f);
		for (int i = 0; i < idsAsArray.length; i++) {
			for (int j = i + 1; j < idsAsArray.length; j++) {
				pairWriter.println(String.valueOf(idsAsArray[i]) + "    " + String.valueOf(idsAsArray[j]));
			}
		}
		pairWriter.close();
	}

	public List<ComparisonResult> readComparisonFile(String comparisonFile) {
		List<ComparisonResult> comparisons = new ArrayList<ComparisonResult>();

		InputStream fis;
		BufferedReader br;
		String line;

		try {
			fis = new FileInputStream(comparisonFile);
			br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\s+");
				comparisons.add(new ComparisonResult(elements[0], elements[1]));
			}

			// Done with the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comparisons;
	}

	public void generatePossiblePairs2(List<AnnotatedDoc> docs, String pathPrefix) throws IOException {
		List<String> ids = new LinkedList<String>();
		for (AnnotatedDoc doc : docs) {
			String id = doc.getId();
			ids.add(id);
		}
		Object[] idsAsArray = ids.toArray();
		File f = createTextDocAnnotationsFile(pathPrefix + "textPairs.txt");
		PrintWriter pairWriter = new PrintWriter(f);
		for (int i = 0; i < idsAsArray.length; i++) {
			for (int j = i + 1; j < idsAsArray.length; j++) {
				pairWriter.println(String.valueOf(idsAsArray[i]) + "    " + String.valueOf(idsAsArray[j]));
			}
		}
		pairWriter.close();
	}

	public void processAnnotations2(List<AnnotatedDoc> docs, String pathPrefix) throws IOException {
		for (AnnotatedDoc doc : docs) {
			List<Annotatable> annotationsInDoc = doc.getAnnotations();
			File f = createTextDocAnnotationsFile(pathPrefix + doc.getId());
			if (doc.getId().equals("35"))
				System.out.println("Doc found");
			try {
				PrintWriter annsWriter = new PrintWriter(f);
				annsWriter.println(annotationsInDoc.size());
				for (Annotatable annotation : annotationsInDoc) {
					//if (annotation.getWeight() >= 0.1)
						annsWriter.println(annotation.getEntity());
				}
				annsWriter.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
	}

	public void writeCostMatrix(Map<IndividualComparison, Double> costMatrix, String pathPrefix) throws IOException {
		File f = createTextDocAnnotationsFile(pathPrefix);
		PrintWriter matrixWriter = new PrintWriter(f);
		matrixWriter.println(costMatrix.size());
		for (Entry<IndividualComparison, Double> entry : costMatrix.entrySet()) {
			IndividualComparison comp = entry.getKey();
			double value = entry.getValue();
			matrixWriter.println(comp + "\t" + value);
		}
		matrixWriter.close();
	}

	public static Map<IndividualComparison, Double> getCostMatrix(File f, MyOWLOntology o) {
		InputStream fis;
		BufferedReader br;
		String line;

		Map<IndividualComparison, Double> costMatrix = new HashMap<IndividualComparison, Double>();

		try {
			fis = new FileInputStream(f);
			br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
			line = br.readLine(); // First line contains the number of
									// annotations
			int numEntries = Integer.parseInt(line);
			for (int i = 0; i < numEntries; i++) {
				line = br.readLine();
				String[] splittedLine = line.split("\t");
				String individualA = splittedLine[0];
				String individualB = splittedLine[1];
				String simValue = splittedLine[2];
				MyOWLIndividual indA = o.getMyOWLIndividual(individualA);
				MyOWLIndividual indB = o.getMyOWLIndividual(individualB);
				double sim = Double.parseDouble(simValue);
				IndividualComparison comp = new IndividualComparison(indA, indB);
				costMatrix.put(comp, sim);
			}

			// Done with the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return costMatrix;
	}

}
