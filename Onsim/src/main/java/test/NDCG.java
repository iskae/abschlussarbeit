package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.paul.evaluation.corpora.LeeEvalHandler;
import de.paul.util.Paths;

public class NDCG {

	protected static final int RESULTS_TO_COMPARE_FACTOR = 2;
	
	private double cutOff;
	private double ndcgRating;

	public NDCG(double cutOff) {

		this.cutOff = cutOff;
	}
	
	public NDCG() {

		this.cutOff = 0.5;
	}
	
	public double evaluateRanking(Map<Integer, Double> humRanking, Map<Integer, Double> algoRanking,
			int countFactor) {

		int relCount = getRelevantElementsCount(humRanking, cutOff);
		// returned lists from ElasticSearch methods can be very short, so take
		// min of desired results and list size
		int numElem = Math.min(relCount * countFactor, Math.max(algoRanking.size(), 0));
		Map<Integer, Double> subAlgoList = new LinkedHashMap<Integer, Double>();
		int i = 0;
		String human = "Human: ";
		String algo = "Algo: ";
		Iterator<Integer> ht = humRanking.keySet().iterator();
		for (Iterator<Integer> it = algoRanking.keySet().iterator(); i < numElem && it.hasNext(); i++)
		{
			Integer elem = it.next();
			subAlgoList.put(elem, algoRanking.get(elem));
			algo += elem + ", ";
			human += ht.next() + ", ";
		}
		//System.out.println(human);
		//System.out.println(algo);
		return evaluateRanking(humRanking, subAlgoList); 
	}

	public double evaluateRanking(Map<Integer, Double> humRanking, Map<Integer, Double> algoRanking) {

		int relCount = getRelevantElementsCount(humRanking, cutOff);
		if (relCount == 0 && algoRanking.size() == 0)
			ndcgRating = 1.0;
		else if (algoRanking.size() == 0 && relCount > 0)
			ndcgRating = 0.0;
		else
			ndcgRating = ndcg(humRanking, algoRanking);
		return getNdcgRating();
	}
	
	public static int getRelevantElementsCount(
			Map<Integer, Double> humRanking, double cutOff) {

		int res = 0;
		for (Integer el : humRanking.keySet()) {
			if (humRanking.get(el) >= cutOff)
				res++;
		}
		return res;
	}
	
	private double ndcg(Map<Integer, Double> humRanking, Map<Integer, Double> algoRanking) {

		// get the human evaluation for the algo ranking
		Double[] rel = humanRelevanceForAlgoRanking(humRanking, algoRanking);
		// numerator
		double dcg = dcg(rel);
		// denominator
		double idcg = idcg(humRanking, algoRanking.size());
		// if it is not zero (double style)
		if (Math.abs(idcg) >= 0.0001)
			return dcg / idcg;
		else
			return 0;
	}
	
	private Double[] humanRelevanceForAlgoRanking(Map<Integer, Double> humRanking,
			Map<Integer, Double> algoRanking) {

		Double[] rel = new Double[algoRanking.size()];
		
		int i = 0;
		for (Integer a_i: algoRanking.keySet()) {
		//for (int i = 0; i < algoRanking.size(); i++) {
			//int a_i = algoRanking.get(i);
			//int pos = 0;
			//if ((pos = humRanking.indexOf(a_i)) != -1) {
			if (humRanking.get(a_i) != null){
				rel[i] = humRanking.get(a_i);
				//i++;
			} else {
				rel[i] = 0.0;
				System.out.println("Algo returned document that wasn't rated by humans");
			}
			i++;
		}
		return rel;
	}
	
	private double dcg(Double[] rel) {

		double score = 0;
		for (int i = 0; i < rel.length; i++) {
			double discounter = Math.log(i + 2) / Math.log(2);
			double denom;
			denom = Math.pow(2, rel[i]) - 1;
			score += denom / discounter;
		}
		return score;
	}
	
	private double idcg(Map<Integer, Double> humRanking, int firstN) {

		Double[] rel_hum = filterList(humRanking, firstN);
		
		double dcg_sorted = dcg(rel_hum);
		return dcg_sorted;
	}
	
	/*
	 * filters out ranking after the first <parameter> places
	 */
	private Double[] filterList(Map<Integer, Double> humRanking, int count) {

		ArrayList<Double> rel2 = new ArrayList<Double>();
		int i = 0;
		for (Integer el : humRanking.keySet()) {

			// OR if there are more relevant documents than were found!
			if (i < count || humRanking.get(el) >= cutOff)
				rel2.add(humRanking.get(el));
			else
				break;
			i++;
		}
		return rel2.toArray(new Double[] {});
	}
	
	public double getNdcgRating() {
		return ndcgRating;
	}
	
	public static void main(String[] args) throws IOException {
		LeeEvalHandler evalHandler = LeeEvalHandler.getInstance(Paths.PINCOMBE_EVAL);
		List<Entry<Integer, Double>> rank = evalHandler.getRanking(2, 3);
		LinkedHashMap<Integer, Double> rankMap = new LinkedHashMap<Integer, Double>();
		for (Entry<Integer, Double> ent: rank)
		{
			rankMap.put(ent.getKey(), ent.getValue());
		}

	}

}
