package test;

import java.util.LinkedList;
import java.util.List;

public class TextDoc {
	private int id;
	private String text;
	private List<TextDocAnnotation> annotationsInTextDoc;

	public TextDoc(int id, String text) {
		this.id = id;
		this.text = text;
		this.annotationsInTextDoc = new LinkedList<TextDocAnnotation>();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<TextDocAnnotation> getAnnotationsInTextDoc() {
		return annotationsInTextDoc;
	}

	public void addAnnotation(TextDocAnnotation ann) {
		this.annotationsInTextDoc.add(ann);
	}

}
