package test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.jena.ontology.Individual;
import org.semanticweb.owlapi.model.OWLIndividual;

import ontologyManagement.MyOWLIndividual;

public class IndividualComparison {
	private MyOWLIndividual individualA;
	private MyOWLIndividual individualB;
	private int hash;

	public IndividualComparison(MyOWLIndividual a, MyOWLIndividual b) {
		individualA = a;
		individualB = b;
		String nameA = a.toString();
		String nameB = b.toString();
		// Compare individuals by name hash ??
		if (nameA.compareTo(nameB) < 0)
			hash = a.hashCode() ^ b.hashCode();
		else
			hash = b.hashCode() ^ a.hashCode();
	}

	public String toString() {
		Locale locale = new Locale("en", "US");
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
		formatter.applyPattern("#0.00000000");
		return individualA + "\t" + individualB;
	}

	public MyOWLIndividual getIndividualA() {
		return individualA;
	}

	public MyOWLIndividual getIndividualB() {
		return individualB;
	}

	public boolean equals(Object o) {
		if (o instanceof IndividualComparison)
			return equals((IndividualComparison) o);
		return false;
	}

	public boolean equals(IndividualComparison b) {
		return individualA.equals(b.individualA) && individualB.equals(b.individualB)
				|| individualA.equals(b.individualB) && individualB.equals(b.individualA);
	}

	public int hashCode() {

		return hash;
	}

}
