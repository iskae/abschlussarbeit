package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONHandler {
	private String jsonPath;
	private Set<String> individuals;
	private Map<TextDoc, List<TextDocAnnotation>> annotatedDocuments;

	public JSONHandler(String path) {
		this.jsonPath = path;
		this.individuals = new HashSet<String>();
		this.annotatedDocuments = new HashMap<TextDoc, List<TextDocAnnotation>>();
	}

	public Set<String> getIndividuals() {
		BufferedReader br = null;
		String fileText = null;
		try {
			br = new BufferedReader(new FileReader(jsonPath));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				line = line.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
				line = line.replaceAll("\\+", "%2B");
				line = URLDecoder.decode(line, "utf-8");
				sb.append(line);
			}
			fileText = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (fileText != null) {
			// get array of docs
			JSONArray arr = new JSONArray(fileText);
			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				String text = obj.getString("text");
				JSONArray ann = obj.getJSONArray("annotations");
				List<TextDocAnnotation> annots = new LinkedList<TextDocAnnotation>();
				for (int j = 0; j < ann.length(); j++) {
					JSONObject annj = ann.getJSONObject(j);
					String ent = annj.getString("entity");
					// double w = annj.getDouble("weight"); // is it necessary ?
					individuals.add(ent);
					annots.add(new TextDocAnnotation(ent));
				}
				annotatedDocuments.put(new TextDoc(i, text), annots);
			}
		}
		return individuals;
	}

	public Map<TextDoc, List<TextDocAnnotation>> getDocs() {
		if (this.annotatedDocuments.size() < 1) {
			this.getIndividuals();
		}
		return this.annotatedDocuments;
	}

}
