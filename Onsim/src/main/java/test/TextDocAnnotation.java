package test;

public class TextDocAnnotation {
	private String uri;

	public TextDocAnnotation(String uri) {
		this.uri = uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
