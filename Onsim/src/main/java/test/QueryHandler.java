package test;

import java.util.List;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.WebContent;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;

public class QueryHandler {
	private String service;

	public QueryHandler() {
		this.service = "http://dbpedia.org/sparql";
	}

	public Query tripleQuery(String individual) {
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString();
		queryStr.append("CONSTRUCT { <");
		queryStr.append(individual);
		queryStr.append("> ?p ?o.}");
		queryStr.append("WHERE { <");
		queryStr.append(individual);
		queryStr.append("> ?p ?o.}");
		return queryStr.asQuery();
	}

	public Query ancestorsQuery(String individual) {
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString();
		queryStr.append("CONSTRUCT { <");
		queryStr.append(individual);
		queryStr.append("> ?p ?o. ?o a ?o1}");
		queryStr.append("WHERE { <");
		queryStr.append(individual);
		queryStr.append("> ?p ?o . ?o a ?o1}");
		return queryStr.asQuery();
	}

	public Model executeQuery(Query triplesQuery, Query ancestorsQuery, Model m) {
		QueryEngineHTTP ancestorsExec = QueryExecutionFactory.createServiceRequest(service, ancestorsQuery);
		QueryEngineHTTP triplesExec = QueryExecutionFactory.createServiceRequest(service, triplesQuery);
		ancestorsExec.setModelContentType("application/rdf+xml");
		triplesExec.setModelContentType("application/rdf+xml");//

		try {
			m = m.union(ancestorsExec.execConstruct());
			m = m.union(triplesExec.execConstruct());
			// Iterator<Triple> results = exec.execConstructTriples();
			return m;
		} finally {
			triplesExec.close();
			ancestorsExec.close();
		}

	}
}
