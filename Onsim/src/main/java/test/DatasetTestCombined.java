package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

import de.paul.annotations.AncestorAnnotation;
import de.paul.annotations.Annotatable;
import de.paul.annotations.NeighborhoodAnnotation;
import de.paul.annotations.SemanticallyExpandedAnnotation;
import de.paul.db.JSONDocSourceLoader;
import de.paul.documents.AnnotatedDoc;
import de.paul.evaluation.corpora.CorpusEvalHandler;
import de.paul.evaluation.corpora.LeeEvalHandler;
import de.paul.evaluation.corpora.RankingEvalHandler;
import de.paul.evaluation.corpora.STSEvalHandler;
import de.paul.kb.dbpedia.DBPediaHandler;
import de.paul.kb.dbpedia.categories.WikiCatHierarchyHandler;
import de.paul.similarity.bipartiteGraphs.TaxonomicScoring;
import de.paul.similarity.bipartiteGraphs.TaxonomicScoring.ScoreMode;
import de.paul.util.CombineMode;
import de.paul.util.Directionality;
import de.paul.util.Paths;
import ontologyManagement.MyOWLIndividual;
import ontologyManagement.MyOWLOntology;
import ontologyManagement.OWLLink;
import similarity.InformationContent;
import similarity.matching.AnnSim;

public class DatasetTestCombined {
	private static final Logger log = Logger.getLogger(DatasetTestCombined.class.getName());

	public static File createTextDocAnnotationsFile(String path) throws IOException {
		File file = new File(path);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		return file;
	}

	public static Set<MyOWLIndividual> getOntologyTerms(List<ComparisonResult> comparisons, String[] files,
			MyOWLOntology m) {
		Set<String> proteins = new HashSet<String>();
		Set<MyOWLIndividual> annotations = new HashSet<MyOWLIndividual>();
		for (Iterator<ComparisonResult> i = comparisons.iterator(); i.hasNext();) {
			ComparisonResult indComp = i.next();
			proteins.add(indComp.getConceptA());
			proteins.add(indComp.getConceptB());
		}
		for (Iterator<String> i = proteins.iterator(); i.hasNext();) {
			String p = i.next();
			for (String file : files) {
				annotations.addAll(DatasetTestDbpedia.getIndividualAnnotations(p, file, m));
			}
		}
		return annotations;
	}

	public static Set<MyOWLIndividual> getIndividualAnnotations(String textDoc, String folder, MyOWLOntology m) {
		File f = new File(folder + "/" + textDoc);
		if (f.exists()) {
			return getAnnotations(f, m);
		}
		return new HashSet<MyOWLIndividual>();
	}

	public static Set<MyOWLIndividual> getAnnotations(File f, MyOWLOntology m) {
		InputStream fis;
		BufferedReader br;
		String line;

		Set<MyOWLIndividual> annotations = new HashSet<MyOWLIndividual>();

		try {
			fis = new FileInputStream(f);
			br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
			line = br.readLine(); // First line contains the number of
									// annotations
			int numAnnotations = Integer.parseInt(line);
			for (int i = 0; i < numAnnotations; i++) {
				line = br.readLine();
				String term = line.split("\t")[0];
				MyOWLIndividual ind = m.getMyOWLIndividual(term);
				if (ind != null)
					annotations.add(ind);
			}

			// Done with the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return annotations;
	}

	public static double getMeanOfMatrix(Map<IndividualComparison, Double> costMatrix) {
		double sum = 0.0;
		double average = 0.0;
		for (Entry<IndividualComparison, Double> entry : costMatrix.entrySet()) {
			sum += entry.getValue();

		}
		average = sum / costMatrix.size();
		return average;
	}

	public static double getVarianceOfMatrix(Map<IndividualComparison, Double> costMatrix, double mean) {
		double sum = 0.0;
		double variance = 0.0;
		for (Entry<IndividualComparison, Double> entry : costMatrix.entrySet()) {
			sum += (entry.getValue() - mean) * (entry.getValue() - mean);
		}
		variance = sum / costMatrix.size();
		return variance;
	}

	public static Map<IndividualComparison, Double> normalizeMatrix(Map<IndividualComparison, Double> costMatrix,
			double mean, double variance) {
		Map<IndividualComparison, Double> normalizedMatrix = new HashMap<IndividualComparison, Double>();
		for (Entry<IndividualComparison, Double> entry : costMatrix.entrySet()) {
			double value = entry.getValue();
			value = (value - mean) / Math.sqrt(variance);
			normalizedMatrix.put(entry.getKey(), value);
		}
		return normalizedMatrix;
	}
	
	public static double avgNeigh(Set<MyOWLIndividual> a)
	{
		double mean = 0;
		
		for (MyOWLIndividual i: a)
		{
			mean += i.getNeighbors().size();
		}
		return mean / a.size();
	}
	
	public static double avgIC(Set<MyOWLIndividual> a)
	{
		double mean = 0;
		
		for (MyOWLIndividual i: a)
		{
			mean += i.getIC();//.getNeighbors().size();
		}
		return mean / a.size();
	}
	
	public static double avgICNeigh(Set<MyOWLIndividual> a)
	{
		double mean = 0;
		int count = 0;
		for (MyOWLIndividual i: a)
		{
			Set<OWLLink> links = i.getNeighbors();
			for (OWLLink o: links)
			{
				mean += ((MyOWLIndividual) o.getDestiny()).getIC();
				count++;
			}
		}
		return mean / count;
	}
	
	public static double varICNeigh(Set<MyOWLIndividual> a)
	{
		double mean = 0;
		int count = 0;
		List<Double> ics = new ArrayList<Double>();
		for (MyOWLIndividual i: a)
		{
			Set<OWLLink> links = i.getNeighbors();
			for (OWLLink o: links)
			{
				double value = ((MyOWLIndividual) o.getDestiny()).getIC();
				count++;
				ics.add(value);
			}
		}
		double[] values = new double[ics.size()];
		for (int i = 0; i < ics.size(); i++)
		{
			values[i] = ics.get(i);
		}
		Variance v = new Variance();
		return v.evaluate(values);
	}
	
	public static double varIC(Set<MyOWLIndividual> a)
	{
		double[] values = new double[a.size()];
		int idx = 0;
		for (MyOWLIndividual i: a)
		{
			values[idx] = i.getIC();
			idx++;
		}
		Variance v = new Variance();
		return v.evaluate(values);
	}
	
	public static double varNeigh(Set<MyOWLIndividual> a)
	{
		double[] values = new double[a.size()];
		int idx = 0;
		for (MyOWLIndividual i: a)
		{
			values[idx] = i.getNeighbors().size();
			idx++;
		}
		Variance v = new Variance();
		return v.evaluate(values);
	}

	public static void main(String[] args) throws Exception {
		String dataset = "STS";
		FileHandler fHandler = new FileHandler();
		DBPediaHandler dbHandler = DBPediaHandler.getInstance(Paths.TDB_DBPEDIA);
		WikiCatHierarchyHandler wikiHandler = WikiCatHierarchyHandler.getInstance(Paths.TDB_DBPEDIA_HIERARCHY);
		JSONDocSourceLoader docLoader = null;
		if (dataset.equals("Lee50"))
		{
			docLoader = new JSONDocSourceLoader(Paths.LEE_ANNOTATED_JSON);
		}
		else
		{
			docLoader = new JSONDocSourceLoader(Paths.STS_ANNOTATED_JSON);
		}
		String prefix = ("src/main/resources/dbpedia/");
		double startRelTime = System.nanoTime();
		List<AnnotatedDoc> docs = docLoader.getAllDocs();
		OntModel model = ModelFactory.createOntologyModel();
		// model.read(dbPediaBaseFile.getAbsolutePath());

		log.info("Adding Individuals to base model...");
		/**
		 * Add Individuals to base DbPedia model. Add types of the object node
		 * in triple separately.
		 */

		AnnotatedDoc.WEIGHT_THD = -1;
		Set<Annotatable> wAnnots = new HashSet<Annotatable>();
		for (AnnotatedDoc doc : docs) {
			List<Annotatable> annots = doc.getAnnotations();
			wAnnots = dbHandler.getNeighborsOutEdges(annots);
			Set<Statement> triples = dbHandler.getStatementNeighborsOutEdges(annots);
			for (Statement triple : triples) {
				Resource subject = model.createResource(triple.getSubject().getURI());
				Resource object = model.createResource(triple.getObject().asResource().getURI());
				Property pred = model.createProperty(triple.getPredicate().getURI());
				ObjectProperty objProp = model.createObjectProperty(pred.toString());
				subject.addProperty(pred, object);
				model.add(subject, objProp, object);
				/*
				 * AncestorAnnotation a = new
				 * AncestorAnnotation(triple.getObject().toString(), 0.0,
				 * dbHandler, wikiHandler); Set<Category> categories =
				 * a.getAncestors(); for (Category c: categories) { Resource r =
				 * model.createResource(c.getEntityName()); }
				 */
				/*
				 * Map<String, List<String>> objectTypes = dbHandler
				 * .getRelevantObjectProperties(triple.getObject().toString());
				 * for (String key : objectTypes.keySet()) { List<String>
				 * objectProperties = objectTypes.get(key); Property prop =
				 * model.createProperty(key); ObjectProperty objTypeProp =
				 * model.createObjectProperty(prop.toString()); for (String s :
				 * objectProperties) { Resource propRes =
				 * model.createResource(s); model.add(object, objTypeProp,
				 * propRes); } }
				 */
				/*
				 * for (Annotatable annot : wAnnots) { WeightedAnnotation wAnnot
				 * = (WeightedAnnotation) annot; Statement triple =
				 * wAnnot.getTriple(); Resource subject =
				 * model.createResource(triple.getSubject().getURI()); Resource
				 * object =
				 * model.createResource(triple.getObject().asResource().getURI()
				 * ); Property pred =
				 * model.createProperty(triple.getPredicate().getURI()); if
				 * (pred.getURI().equals(
				 * "http://dbpedia.org/ontology/ethnicGroup") &&
				 * subject.getURI().equals("http://dbpedia.org/resource/Norway")
				 * ) { System.out.println("Vorsicht");
				 * dbHandler.getNeighborsOutEdges(outEntities) } ObjectProperty
				 * objProp = model.createObjectProperty(pred.toString());
				 * subject.addProperty(pred, object); model.add(subject,
				 * objProp, object); Map<String, List<String>> objectTypes =
				 * dbHandler
				 * .getRelevantObjectProperties(triple.getObject().toString());
				 * for (String key : objectTypes.keySet()) { List<String>
				 * objectProperties = objectTypes.get(key); Property prop =
				 * model.createProperty(key); ObjectProperty objTypeProp =
				 * model.createObjectProperty(prop.toString()); for (String s :
				 * objectProperties) { Resource propRes =
				 * model.createResource(s); model.add(object, objTypeProp,
				 * propRes); } }
				 */

			}
		}

		/**
		 * Write all resources in the model down.
		 */

		// PrintWriter writer = new PrintWriter(prefix + "resources.txt",
		// "UTF-8");
		// NodeIterator resources = model.listObjects();
		//
		// while (resources.hasNext()) {
		// RDFNode node = resources.next();
		// writer.println(node.toString());
		//
		// }
		// writer.close();
		/**
		 * Write the model
		 */
		// model.write(new FileOutputStream(new File(prefix +
		// "dbpediaBaseWithIndWithoutYago.owl")), "RDF/XML");

		/**
		 * Read the model if already created.
		 */
		// model.read(prefix + "dbpediaBaseWithIndWithoutYago.owl");

		/**
		 * Iterate through types of the model.This piece of code was written in
		 * order to get type hierarchies.
		 */

		// File resourcesInOntology = new File(prefix + "resources.txt");
		// BufferedReader br = new BufferedReader(new
		// FileReader(resourcesInOntology));
		// String line = null;
		// while ((line = br.readLine()) != null) {
		// List<Statement> types = dbHandler.getClsHierarchy(line);
		// for (Statement stmt : types) {
		// model.add(stmt);
		// }
		// }
		//
		// br.close();

		/**
		 * Write the newly created ontology down. This ontology is the ontology
		 * that we will use in our code.
		 */

		File newOntologyFile = new File(prefix + "newOntologyWithoutYago.owl");
		File dbPediaBaseFile = new File(prefix + "dbpedia.owl");
		model.read(newOntologyFile.getAbsolutePath());
		Model dbPediaBase = ModelFactory.createDefaultModel();
		dbPediaBase.read(dbPediaBaseFile.getAbsolutePath());

		model.union(dbPediaBase);

		/**
		 * Write the model down.
		 */
		model.write(new FileOutputStream(newOntologyFile));

		MyOWLOntology o = new MyOWLOntology(newOntologyFile.getAbsolutePath(), "http://www.w3.org/2002/07/owl#");
		Set<MyOWLIndividual> annots = new HashSet<MyOWLIndividual>();
		Map<String, Annotatable> mapAnns = new HashMap<String, Annotatable>();
		for (AnnotatedDoc doc : docs) {
			List<Annotatable> annotations = doc.getAnnotations();
			for (Annotatable annot : annotations) {
				MyOWLIndividual ind = o.getMyOWLIndividual(annot.getEntity());
				ind.setWeight(annot.getWeight());
				annots.add(ind);
				mapAnns.put(annot.getEntity(), annot);
			}
		}
		
		
		
		
		// for (MyOWLIndividual annot : annots) {
		// NeighborhoodAnnotation neighbors = new
		// NeighborhoodAnnotation(annot.getURI(), 0.0,
		// DBPediaHandler.getInstance(Paths.TDB_DBPEDIA), 1,
		// Directionality.OUTGOING);
		//
		// Collection<Annotatable> neighsOfAnnot = neighbors.getNeighbors();
		// for (Annotatable annotatable : neighsOfAnnot) {
		// System.out.println(annotatable.getEntity());
		// }
		//
		// int count = neighbors.getNeighbors().size();
		// writer.println(annot.getURI() + "\t" + count);
		// }
		// writer.close();
		
		//Information Content
		
		o.setOWLIndividualLinks(annots, model);
		
		InformationContent ic = new InformationContent(docs, o);
		
		
		/**
		 * Create possible pairs and annotations in file. REMARK: Annotations in
		 * file includes duplicates. They can be removed but does not effect end
		 * result.
		 */
		String[] files = new String[1];
		String comparisonFile = "";
		fHandler.generatePossiblePairs2(docs, prefix);
		if (dataset.equals("Lee50"))
		{
			fHandler.processAnnotations2(docs, prefix + "process_annt/");
			comparisonFile = prefix + "textPairs.txt";
			files[0] =  prefix + "process_annt";
		}
		else
		{
			fHandler.processAnnotations2(docs, "/home/traverso/git/abschlussarbeitEmre/SemRelDocSearch-master/data/STS/annotations/");
			comparisonFile = "/home/traverso/git/abschlussarbeitEmre/SemRelDocSearch-master/data/STS/sentencePairs.txt";
			files[0] =  "/home/traverso/git/abschlussarbeitEmre/SemRelDocSearch-master/data/STS/annotations";
		}
		List<ComparisonResult> comparisons = fHandler.readComparisonFile(comparisonFile);

		

		/**
		 * Create IndividualComparisons. This part creates every possible
		 * individual comparison pairs according to created process_annt/ files.
		 */

		double estimatedRelTime = (System.nanoTime() - startRelTime) / 1000000;
		System.out.println(estimatedRelTime / 1000 / 60);
		Set<IndividualComparison> individualComparisons = new
				HashSet<IndividualComparison>();
		for (Iterator<ComparisonResult> i = comparisons.iterator();
				i.hasNext();) {
			ComparisonResult comp = i.next();
			for (String file : files) {
				Set<MyOWLIndividual> a = getIndividualAnnotations(comp.getConceptA(),
						file, o);
				Set<MyOWLIndividual> b = getIndividualAnnotations(comp.getConceptB(),
						file, o);
				for (MyOWLIndividual c1 : a) {
					for (MyOWLIndividual c2 : b) {
						individualComparisons.add(new IndividualComparison(c1, c2));
					}
				}
			}
		}

		/**
		 * Compute similarity of comparisons and put them in cost matrix.
		 */

		System.out.println("Individual Comparisons: " +
				individualComparisons.size());
		Map<IndividualComparison, Double> costMatrix = new
				HashMap<IndividualComparison, Double>();
		int individualCounter = 0;
		double maxDiff = 0;
		for (IndividualComparison comparison : individualComparisons) {
			System.out.println(individualCounter + " " +
					comparison.getIndividualA().toString() + " compared to "
					+ comparison.getIndividualB().toString());
			Double sim = comparison.getIndividualA().similarityNeighbors(comparison.getIndividualB());
			
			if (sim != -1)
				costMatrix.put(comparison, sim);
			individualCounter++;
		}
		fHandler.writeCostMatrix(costMatrix, prefix + "neighbors2CostMatrix.txt");

		costMatrix = new
				HashMap<IndividualComparison, Double>();
		individualCounter = 0;
		for (IndividualComparison comparison : individualComparisons) {
			System.out.println(individualCounter + " " +
					comparison.getIndividualA().toString() + " compared to "
					+ comparison.getIndividualB().toString());
			Double sim = comparison.getIndividualA().similarityIC(comparison.getIndividualB());
			
			if (sim < 0 || sim > 1)
			{
				System.out.println("ERROR " + sim);
				//sim = comparison.getIndividualA().taxonomicSimilarity(comparison.getIndividualB());
			}
			costMatrix.put(comparison, sim);
			individualCounter++;
		}
		fHandler.writeCostMatrix(costMatrix, prefix + "icCostMatrix.txt");
		
		TaxonomicScoring.mode = ScoreMode.dtax;
		costMatrix = new HashMap<IndividualComparison, Double>();
		individualCounter = 0;
		for (IndividualComparison comparison : individualComparisons) {
			System.out.println(individualCounter + " " +
					comparison.getIndividualA().toString() + " compared to "
					+ comparison.getIndividualB().toString());
			Double sim = comparison.getIndividualA().taxonomicSimilarity(comparison.getIndividualB());
			if (sim < 0 || sim > 1)
			{
				System.out.println("ERROR " + sim);
				//sim = comparison.getIndividualA().taxonomicSimilarity(comparison.getIndividualB());
			}
			costMatrix.put(comparison, sim);
			individualCounter++;
		}
		fHandler.writeCostMatrix(costMatrix, prefix + "taxonomyCostMatrix.txt");

		 
		File neighCostMatrixFile = new File(prefix + "neighbors2CostMatrix.txt");
		File taxonomyCostMatrixFile = new File(prefix + "taxonomyCostMatrix.txt");
		File icCostMatrixFile = new File(prefix + "icCostMatrix.txt");
		Map<IndividualComparison, Double> neighCostMatrix = FileHandler.getCostMatrix(neighCostMatrixFile, o);
		Map<IndividualComparison, Double> taxonomyCostMatrix = FileHandler.getCostMatrix(taxonomyCostMatrixFile, o);
		Map<IndividualComparison, Double> algoCostMatrix = FileHandler.getCostMatrix(taxonomyCostMatrixFile, o);
		Map<IndividualComparison, Double> icCostMatrix = FileHandler.getCostMatrix(icCostMatrixFile, o);
		double avgTax = getMeanOfMatrix(taxonomyCostMatrix);
		System.out.println("The mean of entity comparisons is: " + avgTax);
		double varianceTax = getVarianceOfMatrix(taxonomyCostMatrix, avgTax);
		double avgNeigh = getMeanOfMatrix(neighCostMatrix);
		double varianceNeigh = getVarianceOfMatrix(neighCostMatrix, avgNeigh);
		
		double avgIC = getMeanOfMatrix(icCostMatrix);
		double varianceIC = getVarianceOfMatrix(icCostMatrix, avgNeigh);

		PrintWriter generalWriter = new PrintWriter(prefix + "comparisonsCombinedDtax.txt", "UTF-8");
		//generalWriter.println("TextA\tTextB\tSimilarity\tHumaSim\tAnnA\tAnnB\tAvgNeigh\tVarNeigh\tAvgIC\tVarIC\tAvgICNeigh\tVarICNeigh");
		//generalWriter.println("TextA\tTextB\tSimilarity\tHumaSim\tAnnA\tAnnB\tAvgNeighA\tAvgNeighB\tVarNeighA\tVarNeighB\tAvgICA\tAvgICB\tVarICA\tVarICB");
		generalWriter.println("TextA\tTextB\tSimilarity\tNeighSim\tICSim\tJaccard\tHumaSim\tMaxAnn\tMinAnn\tMaxAvgNeigh\tMinAvgNeigh\tVarNeighA\tVarNeighB\tMaxAvgIC\tMinAvgIC\tVarICA\tVarICB\tMaxAvgICNeigh\tMinAvgICNeigh");
		Map<String, PrintWriter> writers = new HashMap<String, PrintWriter>();
		for (String file : files) {
			if (dataset.equals("Lee50"))
				writers.put(file, new PrintWriter(prefix + file.replaceAll(prefix, "") + "/resultsCombinedDtax.txt"));
			else
				writers.put(file, new PrintWriter(prefix + "/resultsCombinedDtax.txt"));
		}



		PrintWriter entities = new PrintWriter(prefix + "/entityComparison.txt");
		entities.println("EntityA\tEntityB\tAlgo\tGBSS\tDtax\tNeigh\tIC");
		for (Entry<IndividualComparison, Double> entry : algoCostMatrix.entrySet()) {
			IndividualComparison taxComp = entry.getKey();
			double taxSim = entry.getValue();
			Double neighSim = neighCostMatrix.get(taxComp);
			Double icSim = icCostMatrix.get(taxComp);
			
			if (dataset.equals("Lee50"))
			{
				if (neighSim == null)
					entry.setValue(taxSim * icSim);// / 2);
				else
					entry.setValue((taxSim * icSim + neighSim) / 2);
			}
			else
			{
				if (neighSim == null)
					entry.setValue(taxSim);
				else
					entry.setValue((taxSim + neighSim) / 2);
			}
			//}
			
			///////GBSS COMPARISON/////////////
			/*Annotatable a = mapAnns.get(taxComp.getIndividualA().getURI());
			Annotatable b = mapAnns.get(taxComp.getIndividualB().getURI());
			
			AncestorAnnotation aA = taxComp.getIndividualA().getAncestorAnnotation();
			AncestorAnnotation aB = taxComp.getIndividualB().getAncestorAnnotation();
			
			NeighborhoodAnnotation nA = new NeighborhoodAnnotation(a.getEntity(), a.getWeight(), dbHandler, 1, Directionality.OUTGOING);
			NeighborhoodAnnotation nB = new NeighborhoodAnnotation(b.getEntity(), b.getWeight(), dbHandler, 1, Directionality.OUTGOING);
			SemanticallyExpandedAnnotation sA = new SemanticallyExpandedAnnotation(aA, nA, CombineMode.PLUS);
			SemanticallyExpandedAnnotation sB = new SemanticallyExpandedAnnotation(aB, nB, CombineMode.PLUS);
			double achValue = sA.createEdge(sB).leftScore();
			entities.println(taxComp.getIndividualA().getURI() + "\t" + taxComp.getIndividualB().getURI() + "\t" + entry.getValue() + "\t" + achValue + "\t" + taxSim + "\t" + neighSim + "\t" + icSim);*/
			///////////////////////
			
		}
		entities.close();
		int counter = 0, total = comparisons.size();

		/**
		 * Compute similarity between documents.
		 */
		ArrayList<Double> humanScores = new ArrayList<Double>();
		ArrayList<Double> jaccardScores = new ArrayList<Double>();
		ArrayList<Double> neighScores = new ArrayList<Double>();
		ArrayList<Double> taxScores = new ArrayList<Double>();
		ArrayList<Double> icScores = new ArrayList<Double>();
		RankingEvalHandler evalHandler = null;
		if (dataset.equals("Lee50"))
		{
			evalHandler = LeeEvalHandler.getInstance(Paths.PINCOMBE_EVAL);
		}
		else
			evalHandler = STSEvalHandler.getInstance(Paths.STS_EVAL);
		
		double[][] algMatrix = new double[docs.size()][docs.size()], humanMatrix = new double[docs.size()][docs.size()];
		
		startRelTime = System.nanoTime();
		AnnSim bpm = new AnnSim();
		for (Iterator<ComparisonResult> i = comparisons.iterator(); i.hasNext();) {
			ComparisonResult comp = i.next();
			double sim = 0;
			double totalEstimatedTime = 0;
			for (String file : files) {
				Set<MyOWLIndividual> a = getIndividualAnnotations(comp.getConceptA(), file, o);
				Set<MyOWLIndividual> b = getIndividualAnnotations(comp.getConceptB(), file, o);
				
				Set<MyOWLIndividual> intersection = new HashSet<MyOWLIndividual>(a);
				intersection.retainAll(b); // Intersection is used for getting
											// common annotations between texts.
				Set<MyOWLIndividual> union = new HashSet<MyOWLIndividual>(a);
				union.addAll(b); // Union is used for putting all annotations in
									// one set.
				double jaccard = intersection.size();
				jaccard = jaccard / union.size();
				double startTime = System.nanoTime();

				double aux = bpm.maximumMatching(a, b, null, null, taxonomyCostMatrix);
				double taxSim = aux;
				taxScores.add(aux);
				
				aux = bpm.maximumMatching(a, b, null, null, neighCostMatrix);
				double neighSim = aux;
				neighScores.add(aux);
				aux = bpm.maximumMatching(a, b, null, null, icCostMatrix);
				double icSim = aux;
				icScores.add(aux);

				double estimatedTime = System.nanoTime() - startTime;
				totalEstimatedTime += estimatedTime / 1000000;
				double algoSim = bpm.maximumMatching(a, b, null, null, algoCostMatrix);
				//double algoSim = taxSim * icSim + neighSim;
				sim = algoSim;
				comp.setSimilarity(sim);
				int text1 = Integer.parseInt(comp.getConceptA());
				int text2 = Integer.parseInt(comp.getConceptB());
				algMatrix[text1][text2] = sim;
				algMatrix[text2][text1] = sim;
				writers.get(file).println(comp);
				System.out.println(comp + "\t" + totalEstimatedTime + "\t" + counter++ + "/" + total + "\t" + a.size()
						+ "\t" + b.size());
				
				Double humanScore = 0.0;
				try {
					humanScore = evalHandler.getSimilarity(Integer.parseInt(comp.getConceptA()), Integer.parseInt(comp.getConceptB()));
					if (humanScore == null)
						System.out.println("ERROR: Null similarity");
					if (dataset.equals("Lee50"))
						humanScore = (humanScore - 1) / 4 ;
					else
						humanScore = humanScore / 5;
				} catch (IOException e) {
					e.printStackTrace();
				}
				//generalWriter.println(comp + "\t" + humanScore + "\t" + Math.max(a.size(), b.size()) + "\t" + Math.min(a.size(), b.size()) + "\t" + avgNeigh(union) + "\t" + varNeigh(union) + "\t" + avgIC(union) + "\t" + varIC(union) + "\t" + avgICNeigh(union) + "\t" + varICNeigh(union));
				double auxA, auxB;
				auxA = avgNeigh(a);
				auxB = avgNeigh(b);
				double minAvgNeigh = Math.min(auxA, auxB);
				double maxAvgNeigh = Math.max(auxA, auxB);
				
				auxA = avgIC(a);
				auxB = avgIC(b);
				double minAvgIC = Math.min(auxA, auxB);
				double maxAvgIC = Math.max(auxA, auxB);
				
				auxA = avgICNeigh(a);
				auxB = avgICNeigh(b);
				double minAvgICNeigh = Math.min(auxA, auxB);
				double maxAvgICNeigh = Math.max(auxA, auxB);
				
				generalWriter.println(comp + "\t" + neighSim + "\t" + icSim + "\t" + jaccard + "\t" + humanScore + "\t" + Math.max(a.size(), b.size()) + "\t" + Math.min(a.size(),b.size()) + "\t" + maxAvgNeigh + "\t" + minAvgNeigh + "\t" + varNeigh(a) + "\t" + varNeigh(b) + "\t" + maxAvgIC + "\t" + minAvgIC + "\t" + varIC(a) + "\t" + varIC(b) + "\t" + maxAvgICNeigh + "\t" + minAvgICNeigh);
				humanScores.add(humanScore);
				humanMatrix[text1][text2] = humanScore;
				humanMatrix[text2][text1] = humanScore;
				jaccardScores.add(jaccard);
			}
		}
		generalWriter.close();
		for (String file : files) {
			writers.get(file).close();
		}
		estimatedRelTime = (System.nanoTime() - startRelTime) / 1000000;
		System.out.println(estimatedRelTime / 1000 / 60);
		
		o.disposeReasoner();

		double mean = 0;
		int k = 0;
		double[] humanScoresArray = new double[humanScores.size()];
		double[] algoScoresArray = new double[humanScores.size()];
		double max = 0;
		for (Iterator<ComparisonResult> i = comparisons.iterator(); i.hasNext();) {
			ComparisonResult comp = i.next();
			algoScoresArray[k] = comp.getSimilarity();
			if (algoScoresArray[k] > max)
			{
				max = algoScoresArray[k];
				//System.out.println(comp.getConceptA() + " " + comp.getConceptB());
			}
			mean += algoScoresArray[k];
			humanScoresArray[k] = humanScores.get(k);
			k++;
		}
		mean = mean /comparisons.size();
		System.out.println("The mean is " + mean);
		System.out.println("The max is " + max);
		double pearsScore = new PearsonsCorrelation().correlation(algoScoresArray, humanScoresArray);
		System.out.println("The pearson coefficient is " + pearsScore);
		
		PrintWriter rfile = new PrintWriter(prefix + "rfile1.txt");
		rfile.println("Text1\tText2\tAlgo\tHuman\tJaccard\tDps\tNeigh\tIC");
		for (int i = 0; i < algoScoresArray.length; i++)
		{
			rfile.println(/*algoScoresArray[i]*/ comparisons.get(i) + "\t" + humanScoresArray[i] + "\t" + jaccardScores.get(i) + "\t" + taxScores.get(i) + "\t" + neighScores.get(i)  + "\t" + icScores.get(i));
		}
		rfile.close();
		
		System.out.println(nDCG(algMatrix, humanMatrix, evalHandler));
	}

	private static double nDCG(double[][] alg, double[][] human, RankingEvalHandler evalHandler) throws IOException
	{
		//LeeEvalHandler evalHandler = LeeEvalHandler.getInstance(Paths.PINCOMBE_EVAL);
		double mean = 0;
		for (int i = 0; i < alg.length; i++)
		{
			List<Entry<Integer, Double>> rank = evalHandler.getRanking(i, 0);
			LinkedHashMap<Integer, Double> humanMap = new LinkedHashMap<Integer, Double>();
			for (Entry<Integer, Double> ent: rank)
			{
				humanMap.put(ent.getKey(), human[i][ent.getKey()]);
			}
			
			Map<Integer, Double> algo = new HashMap<Integer, Double>();
			for (int j = 0; j < alg.length; j++)
			{
				if (i != j)
				{
					algo.put(j, alg[i][j]);
				}
			}
			Map<Integer, Double> sortedAlg = MapUtil.sortByValue(algo);
			NDCG ndcg = new NDCG();
			double valueNDCG = ndcg.evaluateRanking(humanMap, sortedAlg, 2);
			System.out.println(valueNDCG);
			mean += valueNDCG;
		}
		return mean / alg.length;
	}
	
}
