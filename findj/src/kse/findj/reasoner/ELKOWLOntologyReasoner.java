package kse.findj.reasoner;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import kse.findj.edg.data.MyAxiomRepository;

import org.semanticweb.elk.owl.exceptions.ElkException;
import org.semanticweb.elk.owl.interfaces.ElkAxiom;
import org.semanticweb.elk.owl.interfaces.ElkClass;
import org.semanticweb.elk.owlapi.ElkReasoner;
import org.semanticweb.elk.owlapi.ElkReasonerConfiguration;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.elk.reasoner.Reasoner;
import org.semanticweb.elk.reasoner.taxonomy.model.Taxonomy;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.InferenceType;

import de.tudresden.inf.lat.jcel.core.algorithm.rulebased.RuleBasedProcessor;
import de.tudresden.inf.lat.jcel.core.graph.IntegerRelationMapImpl;
import de.tudresden.inf.lat.jcel.core.graph.IntegerSubsumerGraph;
import de.tudresden.inf.lat.jcel.coreontology.axiom.NormalizedIntegerAxiom;
import de.tudresden.inf.lat.jcel.coreontology.datatype.OntologyExpressivity;
import de.tudresden.inf.lat.jcel.ontology.axiom.complex.ComplexIntegerAxiom;
import de.tudresden.inf.lat.jcel.ontology.axiom.extension.ComplexAxiomExpressivityDetector;
import de.tudresden.inf.lat.jcel.ontology.axiom.extension.IntegerOntologyObjectFactory;
import de.tudresden.inf.lat.jcel.ontology.axiom.extension.IntegerOntologyObjectFactoryImpl;
import de.tudresden.inf.lat.jcel.ontology.normalization.OntologyNormalizer;
import de.tudresden.inf.lat.jcel.owlapi.translator.Translator;

public class ELKOWLOntologyReasoner {
	private ElkReasoner elkReasoner = null;
	private OWLOntology o;
	private Set<NormalizedIntegerAxiom> normalizedIntegerAxiomSet = null;
	private Set<Integer> originalClasses = null;
	private Set<Integer> originalProperties = null;
	private Map<Integer , OWLClass> classMap = null;         
	private Map<OWLClass, Integer> classInvMap = null;
	private Map<String, Integer> classInvMapInString = null;
	private Map<Integer, OWLObjectProperty> objectPropertyMap;
	private Map<OWLObjectProperty, Integer> objectPropertyInvMap;
	private IntegerSubsumerGraph classGraph = null;
	private IntegerRelationMapImpl relationSet = null;
	private Translator translator;
	private Set<ComplexIntegerAxiom> integerOntology;
	private RuleBasedProcessor processor;
	
	public ELKOWLOntologyReasoner(OWLOntology ontology)
	{
		Date start = new Date();
		o = ontology;
		
		this.translator = new Translator(o.getOWLOntologyManager().getOWLDataFactory(), new IntegerOntologyObjectFactoryImpl());
		
		classMap = new HashMap<Integer , OWLClass>();
		classInvMap = new HashMap<OWLClass, Integer>();
		objectPropertyMap = new HashMap<Integer, OWLObjectProperty>();
		objectPropertyInvMap = new HashMap<OWLObjectProperty, Integer>();
		originalClasses = new HashSet<Integer>();
		originalProperties = new HashSet<Integer>();
		
		ElkReasonerFactory reasonerFactory = new ElkReasonerFactory(); 
		ElkReasonerConfiguration configuration = new ElkReasonerConfiguration();
		elkReasoner =  (ElkReasoner) reasonerFactory.createReasoner(ontology, configuration);
		
		integerOntology = getIntegerOntology();
		normalizedIntegerAxiomSet = getNormalizedIntegerAxioms(integerOntology);
		createProcessor();
		classMap = translator.getTranslationRepository().getClassMap();
		classInvMap = translator.getTranslationRepository().getClassInvMap();
		objectPropertyMap = translator.getTranslationRepository().getObjectPropertyMap();
		objectPropertyInvMap = translator.getTranslationRepository().getObjectPropertyInvMap();
		
		Set<OWLClass> classes = classInvMap.keySet();
		classInvMapInString = new HashMap<String, Integer>();
		for(OWLClass owlClass : classes){
			classInvMapInString.put(owlClass.getIRI().toString(), classInvMap.get(owlClass));
		}
		
		classGraph = processor.getClassGraph();
		relationSet = processor.getRelationSet();
		Date end = new Date();
		System.out.println("Loading and normalizing time: " + (end.getTime() - start.getTime()) / 1000 + "s.");	
	}
	
	public ELKOWLOntologyReasoner(ElkReasoner reasoner, OWLOntology ontology)
	{
		Date start = new Date();
		o = ontology;
		
		this.translator = new Translator(o.getOWLOntologyManager().getOWLDataFactory(), new IntegerOntologyObjectFactoryImpl());
		
		classMap = new HashMap<Integer , OWLClass>();
		classInvMap = new HashMap<OWLClass, Integer>();
		objectPropertyMap = new HashMap<Integer, OWLObjectProperty>();
		objectPropertyInvMap = new HashMap<OWLObjectProperty, Integer>();
		originalClasses = new HashSet<Integer>();
		originalProperties = new HashSet<Integer>();
		
		
		elkReasoner =  reasoner;
		
		integerOntology = getIntegerOntology();
		normalizedIntegerAxiomSet = getNormalizedIntegerAxioms(integerOntology);
		createProcessor();
		classMap = translator.getTranslationRepository().getClassMap();
		classInvMap = translator.getTranslationRepository().getClassInvMap();
		objectPropertyMap = translator.getTranslationRepository().getObjectPropertyMap();
		objectPropertyInvMap = translator.getTranslationRepository().getObjectPropertyInvMap();
		
		Set<OWLClass> classes = classInvMap.keySet();
		classInvMapInString = new HashMap<String, Integer>();
		for(OWLClass owlClass : classes){
			classInvMapInString.put(owlClass.getIRI().toString(), classInvMap.get(owlClass));
		}
		
		classGraph = processor.getClassGraph();
		relationSet = processor.getRelationSet();
		Date end = new Date();
		System.out.println("Loading and normalizing time: " + (end.getTime() - start.getTime()) / 1000 + "s.");	
	}
	
	
	public Map<Integer, OWLClass> getClassMap() {
		return classMap;
	}
	
	public void flush()
	{
		Set<OWLAxiom> axioms = elkReasoner.getPendingAxiomAdditions();
		for (OWLAxiom ax : axioms)
		{
			Set<OWLClass> cls = ax.getClassesInSignature();
			
			for (OWLClass c: cls)
			{
				translator.getTranslationRepository().addClass(c);
			}
			Set<ComplexIntegerAxiom> complex = translator.getAxiomTranslator().visit((OWLSubClassOfAxiom)ax);
			Set<NormalizedIntegerAxiom> norm = getNormalizedIntegerAxioms(complex);
			normalizedIntegerAxiomSet.addAll(norm);
			processor.addAxioms(norm);
		}
		elkReasoner.flush();
		
		this.classGraph = processor.getClassGraph();
		this.relationSet = processor.getRelationSet();
	}
	
	public void flush(MyAxiomRepository repository)
	{
		Set<OWLAxiom> axioms = elkReasoner.getPendingAxiomAdditions();
		Set<NormalizedIntegerAxiom> allNorm = new HashSet<NormalizedIntegerAxiom>();
		for (OWLAxiom ax : axioms)
		{
			boolean addAxiom = false;
			Set<OWLClass> cls = ax.getClassesInSignature();
			
			for (OWLClass c: cls)
			{
				if (c.toStringID().contains("testExpl"))
				{
					translator.getTranslationRepository().addClass(c);
					addAxiom = true;
				}
				
			}
			if (addAxiom)
			{
				Set<ComplexIntegerAxiom> complex = translator.getAxiomTranslator().visit((OWLSubClassOfAxiom)ax);
				Set<NormalizedIntegerAxiom> norm = getNormalizedIntegerAxioms(complex);
				allNorm.addAll(norm);
				/*normalizedIntegerAxiomSet.addAll(norm);
				processor.addAxioms(norm);
				repository.flush(norm);*/
			}
		}
		normalizedIntegerAxiomSet.addAll(allNorm);
		processor.addAxioms(allNorm);
		repository.flush(allNorm);
		elkReasoner.flush();
		classMap = translator.getTranslationRepository().getClassMap();
		this.classGraph = processor.getClassGraph();
		this.relationSet = processor.getRelationSet();
	}
	
	public void flush(MyAxiomRepository repository, Set<OWLAxiom> axioms)
	{
		Set<NormalizedIntegerAxiom> allNorm = new HashSet<NormalizedIntegerAxiom>();
		for (OWLAxiom ax : axioms)
		{
			boolean addAxiom = false;
			Set<OWLClass> cls = ax.getClassesInSignature();
			
			for (OWLClass c: cls)
			{
				if (c.toStringID().contains("testExpl"))
				{
					translator.getTranslationRepository().addClass(c);
					addAxiom = true;
				}
				
			}
			if (addAxiom)
			{
				Set<ComplexIntegerAxiom> complex = translator.getAxiomTranslator().visit((OWLSubClassOfAxiom)ax);
				Set<NormalizedIntegerAxiom> norm = getNormalizedIntegerAxioms(complex);
				allNorm.addAll(norm);
				/*normalizedIntegerAxiomSet.addAll(norm);
				processor.addAxioms(norm);
				repository.flush(norm);*/
			}
		}
		normalizedIntegerAxiomSet.addAll(allNorm);
		processor.addAxioms(allNorm);
		repository.flush(allNorm);
		//elkReasoner.flush();
		//classMap = translator.getTranslationRepository().getClassMap();
		this.classGraph = processor.getClassGraph();
		this.relationSet = processor.getRelationSet();
	}
	
	public void dispose()
	{
		elkReasoner.dispose();
	}
	
	public Integer getIntegerClass(String uri){
		Integer a = classInvMapInString.get(uri);
		if (a == null)
			a = translator.getTranslationRepository().getId(o.getOWLOntologyManager().getOWLDataFactory().getOWLClass(IRI.create(uri)));
		return a;
	}
	
	public void doInference(){
		System.out.println("Reasoning start.");
		Date start = new Date();
		elkReasoner.precomputeInferences(InferenceType.CLASS_ASSERTIONS);
		elkReasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		elkReasoner.precomputeInferences(InferenceType.OBJECT_PROPERTY_HIERARCHY);
		elkReasoner.precomputeInferences(InferenceType.OBJECT_PROPERTY_ASSERTIONS);//.classify();
		while (this.processor.process()) {
		}
		
		Date end = new Date();
		System.out.println("Reasoning done.");
		System.out.println("Reasoning cost time: " + (end.getTime() - start.getTime()) / 1000 + "s.");
		
		this.classGraph = processor.getClassGraph();
		this.relationSet = processor.getRelationSet();
	}

	public Set<NormalizedIntegerAxiom> getNormalizedIntegerAxiomSet() {
		
		return normalizedIntegerAxiomSet;
	}

	public Set<Integer> getOriginalClasses() {
		return originalClasses;
	}

	public Set<Integer> getOriginalProperties() {
		return originalProperties;
	}

	public Map<OWLClass, Integer> getClassInvMap() {
		return classInvMap;
	}

	public Map<Integer, OWLObjectProperty> getObjectPropertyMap() {
		return objectPropertyMap;
	}

	public Map<OWLObjectProperty, Integer> getObjectPropertyInvMap() {
		return objectPropertyInvMap;
	}

	public IntegerSubsumerGraph getClassGraph() {
		return classGraph;
	}

	public IntegerRelationMapImpl getRelationSet() {
		return relationSet;
	}

	public Map<String, Integer> getClassInvMapInString() {
		return classInvMapInString;
	}
	
	private Set<ComplexIntegerAxiom> getIntegerOntology() {
		Set<OWLAxiom> owlAxiomSet = new HashSet<OWLAxiom>();
		owlAxiomSet.addAll(this.o.getAxioms());
		for (OWLOntology ont : this.o.getImportsClosure()) {
			owlAxiomSet.addAll(ont.getAxioms());
		}

		this.translator.getTranslationRepository().addAxiomEntities(this.o);

		Set<ComplexIntegerAxiom> ret = this.translator.translateSA(owlAxiomSet);

		updateClassesAndProperties(ret);

		return ret;
	}
	
	private void updateClassesAndProperties(Set<ComplexIntegerAxiom> ret)
	{
		for (ComplexIntegerAxiom axiom : ret) {
			originalClasses.addAll(axiom.getClassesInSignature());
			originalProperties.addAll(axiom.getObjectPropertiesInSignature());
		}
	}
	private Set<NormalizedIntegerAxiom> getNormalizedIntegerAxioms(Set<ComplexIntegerAxiom> ont)
	{
		OntologyNormalizer axiomNormalizer = new OntologyNormalizer();
		Set<NormalizedIntegerAxiom> normalizedAxiomSet = new HashSet<NormalizedIntegerAxiom>(axiomNormalizer.normalize(ont, this.translator.getOntologyObjectFactory()));
		return normalizedAxiomSet;
	}
	
	private void createProcessor()
	{
		OntologyExpressivity expressivity = new ComplexAxiomExpressivityDetector(integerOntology);
		
		IntegerOntologyObjectFactory factory = translator.getOntologyObjectFactory();
		
		processor = new RuleBasedProcessor(
				originalProperties, originalClasses,
				normalizedIntegerAxiomSet, expressivity,
				factory.getNormalizedAxiomFactory(),
				factory.getEntityManager());
	}
	
}
