package kse.findj.test;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLClass;
import de.tudresden.inf.lat.jcel.core.graph.IntegerSubsumerGraph;
import kse.findj.edg.core.ExplanationRoutine;
import kse.findj.edg.core.MasterRoutine;
import kse.findj.edg.data.MyAxiomRepository;
import kse.findj.reasoner.RuleBasedCELReasoner;

public class MainTest {
	
	private static String FILED = "nciOntology";
	/*
	 * The ontology file name.
	 */
	private static String ONTOLOGY = FILED + ".owl";
	/*
	 * The experiment directory.
	 */
	private static String PATH = "D:\\learning\\findJ\\ʵ��\\" + FILED + "\\";
	/*
	 * The repository file.
	 */
	private static String REP = FILED + ".rep";
	/*
	 * The result file.
	 */
	//private static String RESULT = "result.txt";
	/*
	 * The log file.
	 */
	private static String LOG = "log.txt";
	
	private static RuleBasedCELReasoner reasoner;
	
	private static MyAxiomRepository repository;
	
	public static void classifyAndTransform(){
		/*
		 * Create a cel reasoner, get all entailments. 
		 */
		reasoner = new RuleBasedCELReasoner(new File(ONTOLOGY));
		reasoner.doInference();
		
		/*
		 * Transform these entailments to my own types,
		 * and write it to file system.
		 */
		repository = 
				new MyAxiomRepository(
						reasoner.getClassGraph(), 
						reasoner.getRelationSet(),
						reasoner.getNormalizedIntegerAxiomSet()
						);
		repository.createIndex();
		//repository.writeMeOut(PATH + REP);
	}
		
	public static void main(String[] args) {
		classifyAndTransform();
		
		Map<Integer, OWLClass> classMap = reasoner.getClassMap();
		IntegerSubsumerGraph classGragh = repository.getClassGraph();

		/* 
		 * 
		 * Create finding justification routine.
		 * Two mode to do computing:
		 * mode 0: To compute one axiom each time, and you
		 *         should input new axiom to continue
		 *         computing.
		 * mode 1: Compute all justifications for all
		 *         axioms. 
		 */
		int mode;
		Scanner in0 = new Scanner(System.in);
		mode = in0.nextInt();
		if(mode == 0){
			 /* 
			  * Configure the log file.
			  */
			while(true){
				int subClass, supClass;
				System.out.print("Input subClass: ");
				Scanner in1 = new Scanner(System.in);
				subClass = in1.nextInt();
				if(subClass == -1) break;
				System.out.print("Input supClass: ");
				Scanner in2 = new Scanner(System.in);
				supClass = in2.nextInt();
				
				MasterRoutine masterRoutine = new MasterRoutine(repository, 2, subClass, supClass);
				masterRoutine.computeResult();
				
				Set<ExplanationRoutine> justifications = masterRoutine.getJustifications();
				for(ExplanationRoutine expRoutine : justifications){
					Set<Integer> justs = expRoutine.getOriginalAxioms();
					System.out.print("{");
					for(Integer just : justs){
						System.out.print(just + ",");
					}
					System.out.print("}\n");
				}
				
				
			}
		} else if(mode == 1){
			Date start = new Date(); 
			
			int counter = 1;
			int originalCounter = 0;
			//LogWriter logWriter = new LogWriter(PATH + RESULT);
			Collection<Integer> subsumees = classGragh.getElements();
			for(Integer subClass : subsumees){
				Collection<Integer> subsumers = classGragh.getSubsumers(subClass);
				for(Integer supClass : subsumers){
					if(subClass.equals(10702)){
						System.out.println(counter ++ + ": " + subClass + " [= " + supClass);
					}
					
					if(classMap.get(subClass) == null
							|| classMap.get(supClass) == null){
						//System.out.println(counter ++ + ": " + subClass + " [= " + supClass + "*");
						continue;
					}
					
					if(subClass == 0 || supClass == 1) {
						originalCounter ++ ;
						//System.out.println(counter ++ + ": " + subClass + " [= " + supClass + "*");
						continue;
					}
					
					//System.out.println(counter ++ + ": " + subClass + " [= " + supClass);
					MasterRoutine masterRoutine = new MasterRoutine(repository, 2, subClass, supClass);
					masterRoutine.computeResult();
					
					
					
					if(masterRoutine.getJustifications().size() == 0)
						System.exit(-1);
					//logWriter.write(subClass + " [= " + supClass + "\t:" + masterRoutine.getComputingTime());	
				}
			}
			
			Date end = new Date(); 
			System.out.println("Computing time: " + (end.getTime() - start.getTime()) + "ms.");
			System.out.println("Original Count: " + originalCounter);
			//logWriter.closeWriter();
		}
	}

}
