package kse.findj.test.edg;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import kse.findj.edg.core.ExplanationRoutine;
import kse.findj.edg.core.MasterRoutine;
import kse.findj.edg.core.MasterRoutineElk;
import kse.findj.edg.core.MyAxiomRecorder;
import kse.findj.edg.data.AxiomGCI0;
import kse.findj.edg.data.AxiomGCI1;
import kse.findj.edg.data.AxiomGCI2;
import kse.findj.edg.data.AxiomGCI3;
import kse.findj.edg.data.AxiomR;
import kse.findj.edg.data.AxiomRI2;
import kse.findj.edg.data.AxiomRI3;
import kse.findj.edg.data.AxiomS;
import kse.findj.edg.data.MyAxiom;
import kse.findj.edg.data.MyAxiomRepository;
import kse.findj.edg.data.MyAxiomRepositoryElk;
import kse.findj.reasoner.ELKOWLOntologyReasoner;
import kse.findj.reasoner.RuleBasedCELReasoner;

public class OwnTest {

	public static void main(String[] args) throws OWLOntologyCreationException {
		
		
		
		Map<String, String> ontPrefix = new HashMap<String,String>();
		ontPrefix.put("src/main/resources/dataset3/", "http://purl.org/obo/owl/GO#");
		ontPrefix.put("src/main/resources/dataset32014/", "http://purl.obolibrary.org/obo/");
		ontPrefix.put("src/main/resources/DILS2015/annt_goa_2008/", "http://purl.org/obo/owl/GO#");
		ontPrefix.put("src/main/resources/DILS2015/annt_goa_2010/", "http://purl.obolibrary.org/obo/");
		ontPrefix.put("src/main/resources/DILS2015/annt_goa_2012/", "http://purl.obolibrary.org/obo/");
		ontPrefix.put("src/main/resources/DILS2015/annt_goa_2014/", "http://purl.obolibrary.org/obo/");
		String prefix = "src/main/resources/dataset3/";
		String ontFile = "go.owl";
		
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLOntology ont = manager.loadOntologyFromOntologyDocument(new File(ontFile));
		PrefixManager pm = new DefaultPrefixManager(ontPrefix.get(prefix));
		OWLClass test = factory.getOWLClass(":testExpl0", pm);
		OWLObjectProperty p = factory.getOWLObjectProperty(IRI.create("http://purl.org/obo/owl/OBO_REL#part_of"));
		OWLClass b = factory.getOWLClass(IRI.create("http://purl.org/obo/owl/GO#GO_0009410"));
		OWLObjectSomeValuesFrom relationAxiom = factory.getOWLObjectSomeValuesFrom(p, b);
		Set<OWLClassExpression> equivalents = new HashSet<OWLClassExpression>();
		equivalents.add(test);
		equivalents.add(relationAxiom);
		//OWLEquivalentClassesAxiom equiv = factory.getOWLEquivalentClassesAxiom(equivalents);
		OWLSubClassOfAxiom equiv = factory.getOWLSubClassOfAxiom(relationAxiom, test);
		//manager.addAxiom(ont, equiv);
		
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
		//OWLReasoner hermit =  reasonerFactory.createReasoner(ont);
		
		OWLClass subClass = factory.getOWLClass(IRI.create("http://purl.org/obo/owl/GO#GO_0018911"));
		
		//if (hermit.getSubClasses(relationAxiom, false).containsEntity(subClass))
		//	System.out.println("OK");
		
		ELKOWLOntologyReasoner reasoner = new ELKOWLOntologyReasoner(ont);
		//RuleBasedCELReasoner reasoner = new RuleBasedCELReasoner(ont);
		Logger.getLogger("org.semanticweb.elk").setLevel(Level.WARN);
		reasoner.doInference();
		
		manager.addAxiom(ont, equiv);
		
		MyAxiomRepositoryElk repository = new MyAxiomRepositoryElk(reasoner.getClassGraph(),	reasoner.getRelationSet(),	reasoner.getNormalizedIntegerAxiomSet());
		repository.createIndex();
		
		reasoner.flush(repository);
		
		String subClassURI = "http://purl.org/obo/owl/GO#GO_0018911";
		String propURI = "http://purl.org/obo/owl/OBO_REL#part_of";
		String supClassURI = test.toStringID();//"http://purl.org/obo/owl/GO#GO_0009410";
		
		
		int threadNum = 1;
		MasterRoutine masterRoutine = new MasterRoutine(repository, threadNum, reasoner.getIntegerClass(subClassURI), reasoner.getIntegerClass(supClassURI));		
		masterRoutine.computeResult();
		
		Set<ExplanationRoutine> justifications = masterRoutine.getJustifications();
		MyAxiomRecorder recorder = masterRoutine.getRecorder();
		Map<Integer, OWLClass> classMap = reasoner.getClassMap();
		Map<Integer, OWLObjectProperty> propMap = reasoner.getObjectPropertyMap();
		for(ExplanationRoutine expRoutine : justifications){
			Set<Integer> justs = expRoutine.getOriginalAxioms();
			Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
			System.out.print("{");
			for(Integer just : justs){
				MyAxiom ax = recorder.getAxiomFromId(just);
				OWLAxiom axiom = null;
				if (ax instanceof AxiomGCI0)
				{
					AxiomGCI0 gci0 = (AxiomGCI0) ax;
					
					axiom = factory.getOWLSubClassOfAxiom(classMap.get(gci0.getSubClass()), classMap.get(gci0.getSuperClass()));
				}
				if (ax instanceof AxiomGCI1)
				{
					AxiomGCI1 gci1 = (AxiomGCI1) ax;
					OWLClass x = classMap.get(gci1.getLeftSubClass());
					OWLClass y = classMap.get(gci1.getRightSubClass());
					OWLClass sup = classMap.get(gci1.getSuperClass());
					Set<OWLClass> intersection = new HashSet<OWLClass>();
					intersection.add(x);
					intersection.add(y);
					OWLClassExpression interAx = factory.getOWLObjectIntersectionOf(intersection);
					axiom = factory.getOWLSubClassOfAxiom(interAx, sup);
				}
				if (ax instanceof AxiomGCI2)
				{
					AxiomGCI2 gci2 = (AxiomGCI2) ax;
					OWLClass x = classMap.get(gci2.getSubClass());
					OWLClass y = classMap.get(gci2.getClassInSuperClass());
					OWLObjectProperty prop = propMap.get(gci2.getPropertyInSuperClass());
					OWLClassExpression e = factory.getOWLObjectSomeValuesFrom(prop, y);
					axiom = factory.getOWLSubClassOfAxiom(x, e);
				}
				if (ax instanceof AxiomGCI3)
				{
					AxiomGCI3 gci3 = (AxiomGCI3) ax;
					OWLClass x = classMap.get(gci3.getClassInSubClass());
					OWLClass y = classMap.get(gci3.getSuperClass());
					OWLObjectProperty prop = propMap.get(gci3.getPropertyInSubClass());
					OWLClassExpression e = factory.getOWLObjectSomeValuesFrom(prop, x);
					axiom = factory.getOWLSubClassOfAxiom(e, y);
				}
				if (ax instanceof AxiomRI2)
				{
					AxiomRI2 ri2 = (AxiomRI2) ax;
					OWLObjectProperty x = propMap.get(ri2.getSubProperty());
					OWLObjectProperty y = propMap.get(ri2.getSuperProperty());
					axiom = factory.getOWLSubObjectPropertyOfAxiom(x, y);
				}
				if (ax instanceof AxiomRI3)
				{
					AxiomRI3 ri3 = (AxiomRI3) ax;
					OWLObjectProperty x = propMap.get(ri3.getLeftSubProperty());
					OWLObjectProperty y = propMap.get(ri3.getRightSubProperty());
					OWLObjectProperty z = propMap.get(ri3.getSuperProperty());
					List<OWLObjectProperty> chain = new ArrayList<OWLObjectProperty>();
					chain.add(x);
					chain.add(y);
					axiom = factory.getOWLSubPropertyChainOfAxiom(chain, z);
				}
				if (ax instanceof AxiomR)
				{
					AxiomR r = (AxiomR) ax;
					OWLClass x = classMap.get(r.getSubClass());
					OWLClass y = classMap.get(r.getClassInSuperClass());
					OWLObjectProperty prop = propMap.get(r.getPropertyInSuperClass());
					OWLClassExpression e = factory.getOWLObjectSomeValuesFrom(prop, y);
					axiom = factory.getOWLSubClassOfAxiom(x, e);
				}
				if (ax instanceof AxiomS)
				{
					AxiomS s = (AxiomS) ax;
					OWLClass x = classMap.get(s.getSubClass());
					OWLClass y = classMap.get(s.getSuperClass());
					axiom = factory.getOWLSubClassOfAxiom(x, y);
				}
				if (!axiom.getClassesInSignature().contains(test))
				{
					axioms.add(axiom);
					System.out.println(axiom);
				}
			}
			System.out.print("}\n");
		}
		
		System.out.println(" done.");
		
		manager.removeAxiom(ont, equiv);
	}

}
