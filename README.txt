The software located in this repository has the only experimental purposes. Do not expect it to be reliable or clearly readable. If you have any question, do not hesitate to contact me at traverso@fzi.de

To run the experiments of our approach you have to execute the file DatasetTestCombined.java. The variable called "dataset" is used to specify which dataset you want to test. You have two options "Lee50"[1] or "STS"[2].

[1] Lee, M.D., Pincombe, B.M., & Welsh, M.B. (2005). An empirical evaluation of models of text document similarity. In B.G. Bara, L.W. Barsalou & M. Bucciarelli, (Eds.),  Proceedings of the 27th Annual Conference of the Cognitive Science Society, pp. 1254-1259. Mahwah, NJ: Erlbaum.
[2] https://www.cs.york.ac.uk/semeval-2012/task6/data/uploads/datasets/test-gold.tgz