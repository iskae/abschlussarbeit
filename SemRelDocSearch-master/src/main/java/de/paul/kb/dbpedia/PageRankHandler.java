package de.paul.kb.dbpedia;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.tdb.TDBFactory;

public class PageRankHandler {

	private static final String HAS_RANK1 = "http://purl.org/voc/vrank#hasRank";
	private static final String HAS_RANK2 = "http://purl.org/voc/vrank#rankValue";

	// public static void main(String[] args) throws IOException {
	// PageRankHandler handler = new PageRankHandler();
	//
	// }

	private Dataset ds;

	public PageRankHandler() {
		// create the dataset
		this.ds = TDBFactory.createDataset("C:/dbpedia_pagerank/tdb");
	}

	public double getPageRank(String entity) {

		double pageRank = -1;
		String queryString = "select ?pr where { <" + entity + "> <" + HAS_RANK1 + ">/<" + HAS_RANK2 + "> + ?pr}";
		/*
		 * Run query
		 */
		Query query = QueryFactory.create(queryString);
		QueryExecution qexec = QueryExecutionFactory.create(query, ds);
		try {
			ResultSet results = qexec.execSelect();
			if (results != null) {
				QuerySolution soln = null;
				if (results.hasNext()) {
					soln = results.next();
					if (soln != null) {
						pageRank = soln.get("pr").asLiteral().getFloat();
					}
				}
			}
		} finally {
			qexec.close();
		}
		return pageRank;
	}
}
