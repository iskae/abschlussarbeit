package de.paul.db.quadStore;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.tdb.TDBFactory;

import de.paul.annotations.Annotatable;
import de.paul.annotations.WeightedAnnotation;
import de.paul.db.BulkDocSourceHandler;
import de.paul.documents.AnnotatedDoc;
import de.paul.util.URIConverter;

/**
 * Provides means to access Jena QuadStore that holds documents in the xLime
 * format.
 * 
 * @author Chris
 *
 */
public class XLimeQuadStoreHelper implements BulkDocSourceHandler {

	private Dataset ds;
	private static Map<String, XLimeQuadStoreHelper> insts = new HashMap<String, XLimeQuadStoreHelper>();

	private XLimeQuadStoreHelper(String quadStorePath) {

		this.ds = TDBFactory.createDataset(quadStorePath);
	}

	public static XLimeQuadStoreHelper getInstance(String path) {

		if (insts.get(path) == null) {
			insts.put(path, new XLimeQuadStoreHelper(path));
		}
		return insts.get(path);
	}

	private List<Annotatable> getAnnotations(String docURI) {

		List<Annotatable> annotations = new LinkedList<Annotatable>();
		String qString = "SELECT ?e ?w WHERE {" + "GRAPH <" + docURI + "> {"
				+ "?s <http://xlime-project.org/vocab/hasEntity> ?e. "
				+ "?s <http://xlime-project.org/vocab/hasConfidence> ?w." + "}" + "}";
		Query query = QueryFactory.create(qString);
		QueryExecution qexec = QueryExecutionFactory.create(query, ds);
		try {
			ResultSet rs = qexec.execSelect();
			while (rs.hasNext()) {
				QuerySolution soln = rs.nextSolution();
				String ent = soln.get("e").asNode().getURI();
				if (ent.startsWith("http://en") || ent.startsWith("wikien")) {
					// in the data at hand, they point to WIKIpedia. So prefix
					// is stripped
					// and later DBPedia Resource prefix is added
					char splitChar = '/';
					if (ent.startsWith("wikien"))
						splitChar = ':';
					ent = URIConverter.removePrefix(ent, splitChar);
					double weight = soln.getLiteral("w").getDouble();
					WeightedAnnotation ann = new WeightedAnnotation(URIConverter.makeURI(ent), weight);
					annotations.add(ann);
				}
			}
		} finally {
			qexec.close();
		}
		return annotations;
	}

	private String getText(String docURI) {

		String text = null;
		String qString = "SELECT ?o WHERE {" + "GRAPH <" + docURI + "> {?s <http://rdfs.org/sioc/ns#content> ?o}" + "}";
		Query query = QueryFactory.create(qString);
		QueryExecution qexec = QueryExecutionFactory.create(query, ds);
		try {
			ResultSet rs = qexec.execSelect();
			while (rs.hasNext()) {
				QuerySolution soln = rs.nextSolution();
				text = soln.getLiteral("o").getString();
			}
		} finally {
			qexec.close();
		}
		return text;
	}

	public AnnotatedDoc getDocument(String id) {

		String text = this.getText(id);
		String title = this.getTitle(id);
		List<Annotatable> annots = this.getAnnotations(id);
		return new AnnotatedDoc(text, title, id, annots);
	}

	public List<String> getBulkDocs(int offset, int count) {

		List<String> res = new LinkedList<String>();
		String qString = "SELECT ?g WHERE {" + "GRAPH ?g {}" + "} OFFSET " + offset + " LIMIT " + count;
		Query query = QueryFactory.create(qString);
		QueryExecution qexec = QueryExecutionFactory.create(query, ds);
		try {
			ResultSet rs = qexec.execSelect();
			while (rs.hasNext()) {
				QuerySolution soln = rs.nextSolution();
				String uri = soln.get("g").asNode().getURI();
				res.add(uri);
			}
		} finally {
			qexec.close();
		}
		return res;
	}

	private String getTitle(String docURI) {

		String title = null;
		String qString = "SELECT ?o WHERE {" + "GRAPH <" + docURI + "> {?s <http://purl.org/dc/terms/title> ?o}" + "}";
		Query query = QueryFactory.create(qString);
		QueryExecution qexec = QueryExecutionFactory.create(query, ds);
		try {
			ResultSet rs = qexec.execSelect();
			while (rs.hasNext()) {
				QuerySolution soln = rs.nextSolution();
				title = soln.getLiteral("o").getString();
			}
		} finally {
			qexec.close();
		}
		return title;
	}

}
