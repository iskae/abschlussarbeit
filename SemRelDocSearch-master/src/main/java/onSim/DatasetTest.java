package onSim;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import de.paul.RelDocSearch.SemExpRelDocSearch;
import de.paul.annotations.Annotatable;
import de.paul.db.JSONDocSourceLoader;
import de.paul.documents.AnnotatedDoc;
import de.paul.documents.impl.SemanticallyExpandedDoc;
import de.paul.util.Paths;

public class DatasetTest {
	public static File createTextDocAnnotationsFile(String path) throws IOException {
		File file = new File(path);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		return file;
	}

	public static void main(String[] args) throws IOException {
		JSONDocSourceLoader docLoader = new JSONDocSourceLoader(Paths.LEE_ANNOTATED_CORRECTED_JSON);
		List<AnnotatedDoc> docs = docLoader.getAllDocs();
		SemExpRelDocSearch tdbHandler = new SemExpRelDocSearch(Paths.TDB_DBPEDIA, "pincombenew", "news", 2, 20);
		String pathPrefix = "src/main/resources/dbpedia/process_annt/new/";
		for (AnnotatedDoc doc : docs) {
			List<Annotatable> annots = doc.getAnnotations();
			File f = createTextDocAnnotationsFile(pathPrefix + doc.getId());
			PrintWriter annsWriter = new PrintWriter(f);
			annsWriter.println(annots.size());
			for (Annotatable annot : annots) {
				annsWriter.println(annot.getEntity());
				tdbHandler.getRelatedDocuments(annot.getEntity());
			}
			annsWriter.close();
		}

	}

}
